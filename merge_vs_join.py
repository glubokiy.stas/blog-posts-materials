import timeit

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.axes import Axes


def main():
    sizes_to_test = [100, 1000, 10000, 100000]

    merge_times = []
    join_times = []
    for n in sizes_to_test:
        left = pd.DataFrame(np.random.randint(0, 100, size=(n, 3)), columns=list("ABC"))
        right = pd.DataFrame(np.random.randint(0, 100, size=(n, 3)), columns=list('DEF'))

        num_iters = 10
        mean_merge_time = timeit.timeit(lambda: left.merge(right, left_on="A", right_on="D", how="left"), number=num_iters)
        merge_times.append(mean_merge_time / num_iters)

        mean_join_time = timeit.timeit(lambda: left.set_index("A").join(right.set_index("D")), number=num_iters)
        join_times.append(mean_join_time / num_iters)

    print(merge_times)
    print(join_times)


def plot():
    matplotlib.use("MacOSX")

    sizes = np.array([100, 1000, 10000, 100000])
    join_times = np.array([0.0016017373, 0.0017327556999999993, 0.0302312527, 5.020052065300002])
    merge_times = np.array([0.000902056200000001, 0.0010493399999999987, 0.0617424159, 12.801510565800001])

    ax: Axes
    fig, ax = plt.subplots()
    ax.set_title("Merge vs Join")
    ax.plot(sizes, merge_times / sizes * 1e6, label="Merge", lw=1.5, marker="o", ms=2.5,)
    ax.plot(sizes, join_times / sizes * 1e6, label="Join", lw=1.5, marker="o", ms=2.5,)

    ax.legend()
    ax.set_xscale("log")
    ax.grid(linestyle="dashed")
    ax.set_xlabel("number of rows")
    ax.set_ylabel("execution time per row (microseconds)")
    ax.set_xticks(sizes)

    plt.show()


if __name__ == "__main__":
    # main()
    plot()
