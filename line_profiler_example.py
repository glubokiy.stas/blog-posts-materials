import numpy as np
import pandas as pd


def process_df(df: pd.DataFrame) -> pd.DataFrame:
    df = df.copy()

    df['new_col'] = df.apply(lambda row: np.sum(np.cos(row)), axis=1)

    extra_df = pd.DataFrame({
        'col1': np.random.randint(200, size=1000),
        'col2': np.random.randint(200, size=1000),
    })

    df = df.merge(extra_df, how='left', left_on=1, right_on='col1')

    return df


if __name__ == '__main__':
    process_df(pd.DataFrame(np.random.randint(200, size=(50000, 20))))
